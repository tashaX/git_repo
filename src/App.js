import React, { Component } from 'react';
import axios from 'axios';
import SearchForm from './components/SearchForm';
import Profile from './components/Profile';
import RepDetails from './components/RepDetails';
import { Grid } from 'react-flexbox-grid';
import './App.scss';


class App extends Component {
	constructor() {
		super();
		
		this.state = {
			info: '',
			formData: {
				username: '',
			},
			repitems: null
			
		}
		
		this.handleUserSearch = this.handleUserSearch.bind(this);
		this.handleForm = this.handleForm.bind(this);
		
	}
	
	handleUserSearch(e) {
		e.preventDefault();
		
		axios.get('https://api.github.com/users/'+this.state.formData.username)
		.then(response => this.setState({	
			info: response.data,
		})).catch( (err) => { console.log(err); });
		
		axios.get('https://api.github.com/users/'+this.state.formData.username+'/repos')
		.then(response => this.setState({
			repitems : response.data,
		})).catch((err) => { console.log(err); });
	}
	
	handleForm(e) {
		const obj = this.state.formData;
		obj[e.target.name] = e.target.value;
		this.setState(obj);
	};
	
	
	
  render() {
    return (
      <Grid fluid className="App">
        <header className="App-header">
           <h1 className="App-title">GitHub Repositories</h1>
        </header>
				
				<SearchForm
					formData={this.state.formData}
					handleUserSearch={this.handleUserSearch}
					handleForm={this.handleForm}
				/>
				<div>
					<Profile info={this.state.info} username={this.state.formData.username}/>
				</div>
				<div>			
					<RepDetails repitems={this.state.repitems}/>
				</div>
			
      </Grid>
    );
  }
}

export default App;
