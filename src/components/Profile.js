import React from 'react';
import '../App.scss';
import { Row, Col } from 'react-flexbox-grid';



const Profile = (props) => {
	if (props.info) {
    return (
			<Row className="profile">
				<Col xs={12} mdOffset={6} md={6}>
					<h3>Profile info</h3>
						{props.info.avatar_url ?
							<img className="avatar" src={props.info.avatar_url}
									 alt="Profile"
							/> : null }
					</Col>
					
					<Col xs={12} mdOffset={6} md={6}>	
					{props.username ? <p>Username: {props.username}</p> : null }
					{props.info.public_repos ? <p>Public Repos: {props.info.public_repos}</p>: null }
					</Col>		
			</Row>
		)
	}
	else { return null; }
};

export default Profile;