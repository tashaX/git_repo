import React from 'react';

const SearchForm = (props) => {
	return(
		<form onSubmit={ (e) => props.handleUserSearch(e)}>
			<label>
				<p>Search repositories by username</p>
				<input name="username"
				autoComplete="off"
				type="text"
				placeholder="GitHub username"
				required
				value={props.formData.username}
				onChange={props.handleForm}
				/>
			</label>
			<div>
				<button className="btn" type="submit" >Submit</button>
			</div>
		</form>
	)
};

export default SearchForm;