import React from 'react';
import { Row, Col } from 'react-flexbox-grid';

const RepDetails = (props) => {
	if (props.repitems) {
		return(
			<div>
				<h1>Repositories</h1>
				<Row className="reps">
					{props.repitems.map((repitem) =>	
						<Col xs={12} md={4} key={repitem.id}>
							<div>Link: <a href={repitem.html_url} target="_blank">{repitem.name}</a></div>
							<div>Description: {repitem.description ? <i>{repitem.description}</i> : <i>"No description" </i>}</div> 
							<div><p>Language: {repitem.language}</p></div>
							<div><p>Watchers: {repitem.watchers_count}</p></div>
							<div><p>Forks: {repitem.forks_count}</p></div>
						</Col>
					
					)}
				</Row>
			</div>	
		)
	}
	else {return null; }
};

export default RepDetails;